<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing results
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->text('percent')->nullable();
            $table->timestamps();

            $table->integer('quizze_id')->unsigned()->nullable();
            $table->foreign('quizze_id')->references('id')->on('quizzes')->onDelete('cascade');

        });
        Schema::create('results_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('results_id')->unsigned();
            $table->string('title');
            $table->text('text')->nullable();
            $table->text('frame')->nullable();
            $table->string('locale')->index();


            $table->unique(['results_id', 'locale']);
            $table->foreign('results_id')->references('id')->on('results')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('results_translations');
        Schema::dropIfExists('results');
    }
}
