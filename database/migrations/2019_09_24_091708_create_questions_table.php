<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing questions
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('layout');
            $table->nestedSet();
            $table->timestamps();

            $table->integer('quizze_id')->unsigned()->nullable();
            $table->foreign('quizze_id')->references('id')->on('quizzes')->onDelete('cascade');

        });
        Schema::create('questions_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('questions_id')->unsigned();
            $table->text('question');
            $table->string('locale')->index();


            $table->unique(['questions_id', 'locale']);
            $table->foreign('questions_id')->references('id')->on('questions')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('questions_translations');
        Schema::dropIfExists('questions');
    }
}
