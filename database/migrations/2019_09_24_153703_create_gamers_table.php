<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing gamers
        Schema::create('gamers', function (Blueprint $table) {
                            $table->increments('id');
                                        $table->string('name_lastname');
                            $table->string('email');
                            $table->string('phone');
                            $table->string('id_number');
                            $table->string('fb_profile')->nullable();
                            $table->integer('age')->nullable();
                                        $table->timestamps();
            
                    });
            }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
                Schema::dropIfExists('gamers');
    }
}
