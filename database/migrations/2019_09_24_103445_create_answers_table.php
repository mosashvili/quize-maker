<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing answers
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('correct')->nullable();
            $table->nestedSet();
            $table->timestamps();

            $table->integer('question_id')->unsigned()->nullable();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');

        });
        Schema::create('answers_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('answers_id')->unsigned();
            $table->text('answer');
            $table->text('tip')->nullable();
            $table->string('locale')->index();


            $table->unique(['answers_id', 'locale']);
            $table->foreign('answers_id')->references('id')->on('answers')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('answers_translations');
        Schema::dropIfExists('answers');
    }
}
