<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing history
        Schema::create('history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('result_percentage');
            $table->time('completion_time');
            $table->integer('correct_answer');
            $table->timestamps();

            $table->integer('quiz_id')->unsigned()->nullable();
            $table->foreign('quiz_id')->references('id')->on('quizzes')->onDelete('cascade');

            $table->integer('gamer_id')->unsigned()->nullable();
            $table->foreign('gamer_id')->references('id')->on('gamers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('history');
    }
}
