<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing quizzes
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('register')->nullable();
            $table->datetime('start_time')->nullable();
            $table->tinyInteger('visibility');
            $table->timestamps();

        });
        Schema::create('quizzes_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quizzes_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->string('locale')->index();


            $table->unique(['quizzes_id', 'locale']);
            $table->foreign('quizzes_id')->references('id')->on('quizzes')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes_translations');
        Schema::dropIfExists('quizzes');
    }
}
