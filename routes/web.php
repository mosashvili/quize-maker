<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/','QuizzeController@resendCodes');
Route::get('quiz/{id}', 'QuizzeController@index')->name('quiz');
Route::put('/register', 'QuizzeController@register')->name('register');
Route::put('/login', 'QuizzeController@login')->name('login');
Route::get('game/{quizze}/{gamer}', 'QuizzeController@game')->name('game');
Route::put('history', 'QuizzeController@addHistory')->name('addHistory');

Route::get('/test', function(){ return view('test'); });
