<!DOCTYPE html>
<html lang="{{ App()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>Millab</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
          rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>

        <div class="wrap">

            <div class="game-box" data-register="{{ $quiz->register }}">
                <h2 class="game-title">{{ $quiz->name}}</h2>
                <div class="game-text">
                    {{ $quiz->description }}
                </div>
                <div class="game-steps-box" data-steps="{{ $quiz->questions->count() }}">
                    <div class="row">
                        <div class="col-xl-8 text-middle">
                            @for($i = 1;$i <= $quiz->questions->count();$i++)
                            <span class="{{ $i == 1 ? 'active' : '' }}"></span>
                            @endfor
                        </div>
                        <div class="col-xl-4 text-xl-right">
                            <p><strong>1 </strong> / {{ $quiz->questions->count() }}</p>
                        </div>
                    </div>
                </div>


                @foreach($quiz->questions as $key => $question)
                <div class="step step-{{ $key + 1 }}">
                    <div class="question-box">
                        {!! $question->question !!}
                    </div>
                    @if($question->layout == 1)
                        <div class="answer-box">
                            @foreach($question->answers as $akey => $answer)
                            <div class="answer" data-key="{{ $akey }}" data-true="{{ $answer->correct ? 1 : 0 }}">
                                <div class="item">
                                    {!! $answer->answer !!}
                                    <div class="icon true-icon"><img src="{{ asset('img/true-icon.svg') }}" alt="" /></div>
                                    <div class="icon false-icon"><img src="{{ asset('img/false-icon.svg') }}" alt="" /></div>
                                </div>
                                @if($answer->tip)
                                <div class="tip xl-none tip-{{ $akey }}">
                                    {!! $answer->tip !!}
                                    <div class="clearfix"></div>
                                </div>
                                @endif
                            </div>
                            @endforeach

                        </div>
                    @else
                        <div class="answer-box2">
                            <div class="row">
                                @foreach($question->answers as $akey => $answer)
                                    <div class="col-xl-6 col-xs-12">
                                        <div class="answer" data-key="{{ $akey }}" data-true="{{ $answer->correct ? 1 : 0 }}">
                                            {!! $answer->answer !!}
                                            <div class="icon true-icon"><img src="{{ asset('img/true-icon.svg') }}" alt="" /></div>
                                            <div class="icon false-icon"><img src="{{ asset('img/false-icon.svg') }}" alt="" /></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            @foreach($question->answers as $akey => $answer)
                                @if($answer->tip)
                                <div class="tip xl-none tip-{{ $akey }} col-xl-12">
                                    {!! $answer->tip !!}
                                    <div class="clearfix"></div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                   {{--  <div class="share-socials">
                        <p>Share for your friends</p>
                        <div class="socials">
                            <a href="javascript:;" class="facebook"><i class="fab fa-facebook-f"></i></a>
                            <a href="javascript:;" class="twitter"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div> --}}
                </div>
                @endforeach

                <div class="next-btn">@lang('NEXT')</div>
                <br><br>
                <div class="results">
                    @foreach($quiz->results as $result)
                    <div class="result result-{{ $result->percent }} xl-none">
                        <div class="img-box">
                            {!! $result->frame !!}
                        </div>
                        <h2>{!! $result->title !!}</h2>
                        <div class="rate"><span>6 </span> / {{ $quiz->questions->count() }}</div>
                        <div class="text">
                            {!! $result->text !!}
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="result-time">00:00:00</div>
                <style>
                    .result-time {
                        text-align: center;
                        font-size: 42px;
                        color: #55a1d4;
                        margin: 20px 0;
                    }
                </style>

            </div>

        </div>

<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/events.js') }}"></script>
<script src="{{ asset('js/game.js') }}"></script>

<script>
    game.init(function(point,time,percent){
        @if($quiz->register)
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ route('addHistory') }}",
            async :false,
            data: {time: time, point: point,gamer:{{ $gamer->id }},percent: percent,quiz_id: {{ $quiz->id }} },
            type: "PUT",
            success: function (data) {


            }
        });
        @endif
    });
</script>
</body>
</html>
