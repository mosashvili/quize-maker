<!DOCTYPE html>
<html lang="ka">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>Millab</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
          rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>


<div class="wrap">
    <div class="game-content">

        <div class="start-game-popup register-popup">
            <div class="inner-box">
                <div class="inner">
                    <div class="close-btn"><i class="fas fa-times"></i></div>

                    <div class="register-popup">
                        <div class="head">რეგისტრაცია</div>
                        <div class="bottom">
                            <form action="javascript:;" class="reg_form">
                                @csrf
                                <div class="input-box">
                                    <input required type="text" name="name_lastname" placeholder="სახელი გვარი*"/>
                                </div>
                                <div class="input-box">
                                    <input required type="text" title="only numbers are allowed" maxlength="11" minlength="11" pattern="\d*"
                                           name="id_number" placeholder="პირადი ნომერი*"/>
                                </div>
                                <div class="input-box">
                                    <input required type="email" name="email" placeholder="ელ-ფოსტა*"/>
                                </div>
                                <div class="input-box">
                                    <input required type="text" title="Format 5********" minlength="9" pattern="\d*" maxlength="9" name="phone" placeholder="ტელეფონი*"/>
                                </div>
                                <div class="input-box">
                                    <input type="text" name="fb_profile" placeholder="ფეისბუკის პროფილის ლინკი"/>
                                </div>
                                <div class="input-box">
                                    <select name="age">
                                        <option value="0">ასაკი</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="29">29</option>
                                    </select>
                                </div>
                                <div class="small-info">
                                    მითითებულ მეილზე მოგივათ <b>4 ნიშნა კოდი</b> რისი საშუალებითაც შეძლებთ ქვიზის თამაშს
                                    <br><br>
                                    <span style="color:green"></span>
                                </div>
                                <input type="hidden" name="quizz_id" value="{{ $quizze->id }}"/>
                                <div class="message" style="padding:0"></div>
                                <div class="input-box">
                                    <button type="submit" name="">გაგზავნა</button>
                                </div>
                            </form>
                            @if(new DateTime($quizze->start_time) < new DateTime())
                                <div class="authorization input-box">
                                    <button>ავტორიზაცია</button>
                                </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="start-game-popup auth-popup">
            <div class="inner-box">
                <div class="inner">
                    <div class="close-btn"><i class="fas fa-times"></i></div>

                    <div class="register-popup">
                        <div class="head">ქვიზის დასაწყებად შეიყვანეთ მეილზე გამოგზავნილი 4 ნიშნა კოდი</div>
                        <div class="bottom">
                            <form action="javascript:;" method="POST" class="auth_form">
                                <div class="input-box">
                                    <input type="text"  minlength="4" pattern="\d*" maxlength="4" name="code" placeholder="****"
                                           style="text-align: center;"/>
                                </div>
                                <input type="hidden" name="quizz_id" value="{{ $quizze->id }}"/>
                                <div class="input-box">
                                    <button type="submit" name="">დაწყება</button>
                                </div>
                                <div class="message"></div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="wrap preview">
            <div class="start-game-cont">
                <h2>{{ $quizze->name }}</h2>
                <div class="text">
                    {!! $quizze->description !!}
                </div>
                <div class="box">
                    <div class="img">
                        {!! $quizze->frame  !!}

                    </div>
                    <div class="bg"></div>
                    <div class="inner">
                        <div class="time countdown" data-date="{{ $quizze->start_time }}"></div>
                        @if(new DateTime($quizze->start_time) < new DateTime())
                            <div class="authorization input-box">
                                <a href="javascript:;" class="button">@lang('START')</a>
                            </div>
                        @else
                        <a href="javascript:;" class="button register-now">@lang('REGISTER NOW!')</a>
                        @endif
                        {{-- <a href="javascript:;" class="button red">CLOSED</a> --}}
                    </div>
                </div>
                {{--<div class="share-socials">--}}
                    {{--<p>Share for your friends</p>--}}
                    {{--<div class="socials">--}}
                        {{--<a href="javascript:;" class="facebook"><i class="fab fa-facebook-f"></i></a>--}}
                        {{--<a href="javascript:;" class="twitter"><i class="fab fa-twitter"></i></a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/events.js') }}"></script>

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".reg_form").submit(function () {
        var self = $(this);
        var name = $("input[name=name_lastname]").val();
        var id = $("input[name=id_number]").val();
        var email = $("input[name=email]").val();
        var phone = $("input[name=phone]").val();
        var profile = $("input[name=fb_profile]").val();
        var age = $("select[name=age]").val();
        var quizzId = $("input[name=quizz_id]").val();

        $.ajax({
            url: "{{ route('register') }}",
            async :false,
            headers: { "cache-control": "no-cache" },
            data: {name: name, email: email, phone: phone, profile: profile, id: id, age: age, quizz_id: quizzId},
            type: "PUT",
            success: function (data) {
                if (data.success) {
                    if (data.showLogin) {
                        $('.authorization ').click();
                    }

                    $('.small-info span').text(data.message)
                } else {
                    self.find('.message').text(data.message)
                }
            }
        });
    });

    $(".auth_form").submit(function () {
        var code = $("input[name=code]").val();
        var quizzId = $("input[name=quizz_id]").val();
        var self = $(this);

        $.ajax({
            url: "{{ route('login') }}",
            data: {code: code, quizz_id: quizzId},
            async :false,
            type: "PUT",
            success: function (data) {
                if (data.success) {
                    window.location.replace(data.link);
                } else {
                    self.find('.message').text(data.message)
                }
            }
        });
    });

</script>
</body>
</html>
