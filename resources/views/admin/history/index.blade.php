@extends('betterfly::admin.common.layout')


@push('css')
@endpush

@section('content')
    <main class="main">

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">History</li>
        </ol>

        @if(\Session::get('status'))
            <div class="container-fluid">
                <div id="ui-view">
                    <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
                </div>
            </div>
        @endif
        <div class="container-fluid">
            <div id="ui-view">
                <div>
                    <div class="animated fadeIn">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-edit"></i> History
                                <div class="card-header-actions">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="col-sm-12">
                                        <table id="datatable"
                                               class="table table-striped table-bordered datatable dataTable no-footer datatable dataTable">
                                            <thead>
                                            <tr>
                                                <th>Gamer Name</th>
                                                <th>Phone</th>
                                                <th>Quiz Name</th>
                                                <th>Completion time</th>
                                                <th>Sum of questions</th>
                                                <th>Correct Answer</th>
                                                <th>Result Percentage</th>
                                                {{--<th>Actions</th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data as $key => $item)
                                                <tr role="row"
                                                    class="{{ ($key + 1 % 2) == 0 ? 'odd' : 'even' }}">


                                                    <td class="align-middle">{{
                                                        strip_tags($item->name_lastname) }}
                                                    </td>

                                                    <td class="align-middle">{{
                                                        strip_tags($item->phone) }}
                                                    </td>

                                                    <td class="align-middle">{{
                                                        strip_tags($item->quiz_name) }}
                                                    </td>

                                                    <td class="align-middle">{{
                                                        strip_tags($item->completion_time) }}
                                                    </td>

                                                    <td class="align-middle">{{
                                                        strip_tags($item->questions_sum) }}
                                                    </td>

                                                    <td class="align-middle">{{
                                                        strip_tags($item->correct_answer) }}
                                                    </td>

                                                    <td class="align-middle">{{
                                                        strip_tags($item->result_percentage) }}
                                                    </td>


                                                    {{--<td class="text-center align-middle">--}}


                                                        {{--<a data-url="{{ route("gamers.history.delete",[$Gamer->id,$item->id ]) }}"--}}
                                                           {{--class="btn btn-danger remove-item" href="javascript:;">--}}
                                                            {{--<i class="fa fa-trash-o"></i>--}}
                                                        {{--</a>--}}
                                                    {{--</td>--}}
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="text-right">
                                            <div class="d-inline-block">{{ method_exists($data,'links') ?
                                            $data->links() : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


@push('scripts')
    <script>
        loadCss('../vendor/betterfly/plugins/dataTable/dataTables.bootstrap4.min.css');

        loadScript('../vendor/betterfly/plugins/dataTable/jquery.dataTables.js', dataTableLoaded);

        function dataTableLoaded() {
            loadScript('../vendor/betterfly/plugins/dataTable/dataTables.bootstrap4.js', bootstrapLoaded);

            function bootstrapLoaded() {
                table = $('#datatable').DataTable({
                    "paging": {{ method_exists($data,'links') ? 'false,' : 'true,'
      }}
                        "columnDefs"
            :
                [
                    {
                        "searchable": false,
                        "targets": 0,
                        "sortable": false
                    },
                    {
                        "searchable": false,
                        "targets": 1,
                        "sortable": false
                    },
                    {
                        "searchable": false,
                        "targets": 2,
                        "sortable": false
                    },
                    {
                        "searchable": false,
                        "targets": 3,
                        "sortable": false
                    },
                    {
                        "searchable": false,
                        "targets": 4,
                        "sortable": false
                    },
                    {
                        "searchable": false,
                        "targets": 5,
                        "sortable": false
                    },
                    {
                        "searchable": false,
                        "targets": 6,
                        "sortable": false
                    },
                ]
            })
                ;
            }
        }
    </script>
@endpush
