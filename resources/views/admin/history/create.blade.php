@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                    <li class="breadcrumb-item">
                <a href="{{ route("Gamers.history.index",$Gamer->id) }}">History</a>
            </li>
                <li class="breadcrumb-item active">History</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>History</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("Gamers.history.store",[$Gamer->id]) }}" method="post"
                                          enctype="multipart/form-data">
                                                                                @csrf
                                        @php
    if(isset($data)){
        $value = $data->quiz_id;
        $value = key_exists('quiz_id',old()) ? old('quiz_id') : $value;
    } else{
        $value = old('quiz_id');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Name</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="integer" name="quiz_id" placeholder="Name">
        @if($errors->get('quiz_id'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('quiz_id') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->gamer_id;
        $value = key_exists('gamer_id',old()) ? old('gamer_id') : $value;
    } else{
        $value = old('gamer_id');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Gamer ID</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="integer" name="gamer_id" placeholder="Gamer ID">
        @if($errors->get('gamer_id'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('gamer_id') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->result_percentage;
        $value = key_exists('result_percentage',old()) ? old('result_percentage') : $value;
    } else{
        $value = old('result_percentage');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Result percentage</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="integer" name="result_percentage" placeholder="Result percentage">
        @if($errors->get('result_percentage'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('result_percentage') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->completion_time;
        $value = key_exists('completion_time',old()) ? old('completion_time') : $value;
    } else{
        $value = old('completion_time');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Completion time</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="time" name="completion_time" placeholder="Completion time">
        @if($errors->get('completion_time'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('completion_time') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->correct_answer;
        $value = key_exists('correct_answer',old()) ? old('correct_answer') : $value;
    } else{
        $value = old('correct_answer');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Correct Answers</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="integer" name="correct_answer" placeholder="Correct Answers">
        @if($errors->get('correct_answer'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('correct_answer') }}</div>
        @endif
    </div>
</div>

                                        <input type="hidden" value="\App\Modules\History\HistoryRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection