@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                    <li class="breadcrumb-item">
                <a href="{{ route("gamers.index") }}">Gamers</a>
            </li>
                <li class="breadcrumb-item active">Gamers</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Gamers</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("gamers.update",[$data->id]) }}" method="post"
                                          enctype="multipart/form-data">
                                                                                    @method('PUT')
                                                                                @csrf
                                        @php
    if(isset($data)){
        $value = $data->name_lastname;
        $value = key_exists('name_lastname',old()) ? old('name_lastname') : $value;
    } else{
        $value = old('name_lastname');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Name Lastname</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="string" name="name_lastname" placeholder="Name Lastname">
        @if($errors->get('name_lastname'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('name_lastname') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->email;
        $value = key_exists('email',old()) ? old('email') : $value;
    } else{
        $value = old('email');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Email</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="string" name="email" placeholder="Email">
        @if($errors->get('email'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->phone;
        $value = key_exists('phone',old()) ? old('phone') : $value;
    } else{
        $value = old('phone');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Phone</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="string" name="phone" placeholder="Phone">
        @if($errors->get('phone'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('phone') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->id_number;
        $value = key_exists('id_number',old()) ? old('id_number') : $value;
    } else{
        $value = old('id_number');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">ID Number</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="string" name="id_number" placeholder="ID Number">
        @if($errors->get('id_number'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('id_number') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->fb_profile;
        $value = key_exists('fb_profile',old()) ? old('fb_profile') : $value;
    } else{
        $value = old('fb_profile');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">FB Profile</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="string" name="fb_profile" placeholder="FB Profile">
        @if($errors->get('fb_profile'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('fb_profile') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->age;
        $value = key_exists('age',old()) ? old('age') : $value;
    } else{
        $value = old('age');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Age</label>
    <div class="col-md-9">
        <input   class="form-control" value="{{ $value }}" type="integer" name="age" placeholder="Age">
        @if($errors->get('age'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('age') }}</div>
        @endif
    </div>
</div>

                                        <input type="hidden" value="\App\Modules\Gamers\GamersRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection