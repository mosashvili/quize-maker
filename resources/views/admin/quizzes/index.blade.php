@extends('betterfly::admin.common.layout')


@push('css')
@endpush

@section('content')
    <main class="main">

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Quizzes</li>
        </ol>

        @if(\Session::get('status'))
            <div class="container-fluid">
                <div id="ui-view">
                    <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
                </div>
            </div>
        @endif
        <div class="container-fluid">
            <div id="ui-view">
                <div>
                    <div class="animated fadeIn">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-edit"></i> Quizzes
                                <div class="card-header-actions">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="col-xl-12 text-right">
                                    <a href="{{ route("quizzes.create") }}"
                                       class="btn btn-square btn-success active mb-3"
                                       type="button"
                                       aria-pressed="true">add new Quizze
                                    </a>
                                </div>
                                <div class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="col-sm-12">
                                        <table id="datatable"
                                               class="table table-striped table-bordered datatable dataTable no-footer datatable dataTable">
                                            <thead>
                                            <tr>
                                                <th>Link</th>
                                                <th>Name</th>
                                                <th>Visibility</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data as $key => $item)
                                                <tr role="row"
                                                    class="{{ ($key + 1 % 2) == 0 ? 'odd' : 'even' }}">


                                                    <td class="align-middle">
                                                        {{ URL::to('/').'/'.App()->getLocale().'/quiz/'.$item->id }}
                                                    </td>
                                                    <td class="align-middle">{{
                                                        strip_tags($item->name) }}
                                                    </td>


                                                    <td class="text-center align-middle">
                                                        <label class="switch switch-label switch-success">
                                                            <input data-url="{{ route('set-visibility',['Quizzes',$item->id]) }}"
                                                                   class="switch-input visibility" type="checkbox"
                                                                    {{ $item->visibility ? 'checked' : '' }}>
                                                            <span class="switch-slider" data-checked="On"
                                                                  data-unchecked="Off"></span>
                                                        </label>
                                                    </td>

                                                    <td class="text-center align-middle">
                                                        <a class="btn btn-warning"
                                                           href="{{ route('quizzes.questions.index',$item->id) }}">
                                                            Questions
                                                        </a>
                                                        <a class="btn btn-secondary"
                                                           href="{{ route('quizzes.results.index',$item->id) }}">
                                                            Results Page
                                                        </a>
                                                        <a class="btn btn-success"
                                                           href="{{ route('gamers.history.quiz',$item->id) }}">
                                                            Statistics
                                                        </a>

                                                        <a class="btn btn-info"
                                                           href="{{ route("quizzes.edit",[$item->id]) }}">
                                                            <i class="fa fa-edit"></i>
                                                        </a>

                                                        <a data-url="{{ route("quizzes.delete",[$item->id]) }}"
                                                           class="btn btn-danger remove-item" href="javascript:;">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="text-right">
                                            <div class="d-inline-block">{{ method_exists($data,'links') ?
                                            $data->links() : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection


@push('scripts')
    <script>
        loadCss('../vendor/betterfly/plugins/dataTable/dataTables.bootstrap4.min.css');

        loadScript('../vendor/betterfly/plugins/dataTable/jquery.dataTables.js', dataTableLoaded);

        function dataTableLoaded() {
            loadScript('../vendor/betterfly/plugins/dataTable/dataTables.bootstrap4.js', bootstrapLoaded);

            function bootstrapLoaded() {
                table = $('#datatable').DataTable({
                    "paging": {{ method_exists($data,'links') ? 'false,' : 'true,'
      }}
                        "columnDefs"
            :
                [
                    {
                        "searchable": false,
                        "targets": 0,
                        "sortable": false
                    },
                ]
            })
                ;
            }
        }
    </script>
@endpush
