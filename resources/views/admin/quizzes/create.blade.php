@extends('betterfly::admin.common.layout')

@section('content')

<main class="main">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
                    <li class="breadcrumb-item">
                <a href="{{ route("quizzes.index") }}">Quizzes</a>
            </li>
                <li class="breadcrumb-item active">Quizzes</li>
    </ol>

    @if(\Session::get('status'))
    <div class="container-fluid">
        <div id="ui-view">
            <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
        </div>
    </div>
    @endif
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Quizzes</strong>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal"
                                          action="{{ route("quizzes.store") }}" method="post"
                                          enctype="multipart/form-data">
                                                                                @csrf
                                        @php
    if(isset($data)){
        $value = $data->name;
        $value = key_exists('name',old()) ? old('name') : $value;
    } else{
        $value = old('name');
    }
@endphp



<div class="form-group row ">
    <label class="col-md-3 col-form-label" for="text-input">Name</label>
    <div class="col-md-9">
        <input  required class="form-control" value="{{ $value }}" type="string" name="name" placeholder="Name">
        @if($errors->get('name'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
        @endif
    </div>
</div>
@php
    if(isset($data)){
        $value = $data->description;
        $value = key_exists('description',old()) ? old('description') : $value;
    } else{
        $value = old('description');
    }
@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="text-input">Description</label>
    <div class="col-md-9">
        <textarea rows="5" cols="50" class="form-control" name="description" placeholder="Description">{{ $value }}</textarea>
        @if($errors->get('description'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('description') }}</div>
        @endif
    </div>
</div>@php
if(isset($data)){
$value = $data->frame;
$value = key_exists('frame',old()) ? old('frame') : $value;
} else{
$value = old('frame');
}
@endphp

<div class="form-group row">
    <label class="col-md-3 col-form-label" for="text-input">Frame</label>
    <div class="col-md-9">
        <textarea id="ckEditor_2" name="frame">
            {{ $value }}
        </textarea>
        @if($errors->get('frame'))
        <br>
        <div class="alert alert-danger" role="alert">{{ $errors->first('frame') }}</div>
        @endif
    </div>
</div>

@push('scripts')


<script>
  loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

  loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_2);

  function loadckEditor_2() {
    loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
      $("textarea#ckEditor_2").ckeditor({
        extraPlugins: "image2",
        filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
        filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
      });
    });
  }
</script>
@endpush@php
    if(isset($data)){
        $value = $data->register;
        $value = key_exists('register',old()) ? old('register') : $value;
    } else{
        $value = old('register');
    }
@endphp


<div class="form-group row">
    <label class="col-md-3 col-form-label" for="checkbox-input">Register</label>
    <div class="col-md-9">
        <label class="switch switch-label switch-outline-success-alt">
            <input class="switch-input checkbox-plugin" for="register" type="checkbox" {{ $value ? 'checked' : '' }}>
            <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
        </label>
        <input type="hidden" name="register" value="{{ $value }}">
    </div>
    @if($errors->get('register'))
    <br>
    <div class="alert alert-danger" role="alert">{{ $errors->first('register') }}</div>
    @endif
</div>@php
if(isset($data)){
$value = $data->start_time;
$value = key_exists('start_time',old()) ? old('start_time') : $value;
} else{
$value = old('start_time');
}
@endphp

<div class="form-group row">
    <div class="input-group date">
        <label class="col-md-3 col-form-label" for="date-input">Start Time</label>
        <div class="col-md-9">
            <div class="input-group">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
                <input type="text" autocomplete="off" class="form-control" id="datetimeField_4"
                       value="{{ $value }}" name="start_time"></div>
            @if($errors->get('start_time'))
            <br>
            <div class="alert alert-danger" role="alert">{{ $errors->first('start_time') }}</div>
            @endif
        </div>
    </div>
</div>

@push('scripts')
<script>
  loadCss(['../vendor/betterfly/plugins/dateField/jquery-ui.css', '../vendor/betterfly/plugins/dateField/jquery-ui-timepicker-addon.min.css']);
  loadScript(['../vendor/betterfly/plugins/dateField/jquery-ui.min.js'], loaddatetimeField_4);

  function loaddatetimeField_4() {
    loadScript(['../vendor/betterfly/plugins/dateField/jquery-ui-timepicker-addon.min.js'], function () {
      $("input#datetimeField_4").datetimepicker({dateFormat: 'yy-mm-dd'});
    })
  }
</script>
@endpush


                                        <input type="hidden" value="\App\Modules\Quizzes\QuizzesRequest" name="request_name_space">
                                        <div class=" text-right">
                                            <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection