@extends('betterfly::admin.common.layout')

@section('content')

    <main class="main">

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('quizzes.index') }}">Quizzes</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route("quizzes.questions.index",$quiz->id) }}">Questions</a>
            </li>
            <li class="breadcrumb-item active">Questions</li>
        </ol>

        @if(\Session::get('status'))
            <div class="container-fluid">
                <div id="ui-view">
                    <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
                </div>
            </div>
        @endif
        <div class="container-fluid">
            <div id="ui-view">
                <div>
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Questions</strong>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal"
                                              action="{{ route("quizzes.questions.store",[$quiz->id]) }}" method="post"
                                              enctype="multipart/form-data">
                                            @csrf

                                            @php
                                                if(isset($data)){
                                                $value = $data->question;
                                                $value = key_exists('question',old()) ? old('question') : $value;
                                                } else{
                                                $value = old('question');
                                                }
                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="text-input">Question</label>
                                                <div class="col-md-9">
        <textarea id="ckEditor_1" name="question">
            {{ $value }}
        </textarea>
                                                    @if($errors->get('question'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('question') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            @push('scripts')


                                                <script>
                                                    loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

                                                    loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_1);

                                                    function loadckEditor_1() {
                                                        loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
                                                            $("textarea#ckEditor_1").ckeditor({
                                                                extraPlugins: "image2",
                                                                filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
                                                                filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
                                                            });
                                                        });
                                                    }
                                                </script>
                                            @endpush

                                            @php
                                                if(isset($data)){
                                                    $value = is_a($data->layout,'Illuminate\Database\Eloquent\Collection') ? $data->percent->pluck('id')->toArray() : $data->layout;
                                                    $value = key_exists('layout',old()) ? old('layout') : $value;
                                                } else{
                                                    $value = old('percent');
                                                }
                                            @endphp


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                       for="selectField_3">Layout</label>
                                                <div class="col-md-9">
                                                    <select class="dd col-lg-12 form-control" id="selectField_3"
                                                            name="layout">
                                                        <option {{ $value == 1 ? 'selected' : '' }} value="1">
                                                            Full grid
                                                        </option>
                                                        <option {{ $value == 2 ? 'selected' : '' }} value="2">
                                                            Half Grid
                                                        </option>
                                                    </select>
                                                    @if($errors->get('layout'))
                                                        <br>
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('layout') }}</div>
                                                    @endif
                                                </div>
                                            </div>


                                            @push('scripts')

                                                <script>

                                                    loadCss(['../vendor/betterfly/plugins/selectPlugin/select2.min.css']);
                                                    loadScript(['../vendor/betterfly/plugins/selectPlugin/select2.min.js'], loadselectField_3);

                                                    function loadselectField_3() {
                                                        $("select#selectField_3").select2();
                                                    }
                                                </script>
                                            @endpush


                                            <input type="hidden" value="true"
                                                   name="multilang">
                                            <input type="hidden" value="\App\Modules\Questions\QuestionsRequest"
                                                   name="request_name_space">
                                            <div class=" text-right">
                                                <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                    <i class="fa fa-dot-circle-o"></i> Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection