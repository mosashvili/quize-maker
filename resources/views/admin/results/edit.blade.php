@extends('betterfly::admin.common.layout')

@section('content')

    <main class="main">

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route("quizzes.results.index",$quiz->id) }}">Results</a>
            </li>
            <li class="breadcrumb-item active">Results</li>
        </ol>

        @if(\Session::get('status'))
            <div class="container-fluid">
                <div id="ui-view">
                    <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
                </div>
            </div>
        @endif
        <div class="container-fluid">
            <div id="ui-view">
                <div>
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Results</strong>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal"
                                              action="{{ route("quizzes.results.update",[$quiz->id,$data->id]) }}"
                                              method="post"
                                              enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                                            @php
                                                if(isset($data)){
                                                    $value = $data->title;
                                                    $value = key_exists('title',old()) ? old('title') : $value;
                                                } else{
                                                    $value = old('title');
                                                }
                                            @endphp


                                            <div class="form-group row ">
                                                <label class="col-md-3 col-form-label" for="text-input">Title</label>
                                                <div class="col-md-9">
                                                    <input required class="form-control" value="{{ $value }}"
                                                           type="string" name="title" placeholder="Title">
                                                    @if($errors->get('title'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('title') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            @php
                                                if(isset($data)){
                                                $value = $data->text;
                                                $value = key_exists('text',old()) ? old('text') : $value;
                                                } else{
                                                $value = old('text');
                                                }
                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="text-input">text</label>
                                                <div class="col-md-9">
        <textarea id="ckEditor_1" name="text">
            {{ $value }}
        </textarea>
                                                    @if($errors->get('text'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('text') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            @push('scripts')


                                                <script>
                                                    loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

                                                    loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_1);

                                                    function loadckEditor_1() {
                                                        loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
                                                            $("textarea#ckEditor_1").ckeditor({
                                                                extraPlugins: "image2",
                                                                filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
                                                                filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
                                                            });
                                                        });
                                                    }
                                                </script>
                                            @endpush@php
                                                if(isset($data)){
                                                $value = $data->frame;
                                                $value = key_exists('frame',old()) ? old('frame') : $value;
                                                } else{
                                                $value = old('frame');
                                                }
                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="text-input">Frame</label>
                                                <div class="col-md-9">
        <textarea id="ckEditor_2" name="frame">
            {{ $value }}
        </textarea>
                                                    @if($errors->get('frame'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('frame') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            @push('scripts')


                                                <script>
                                                    loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

                                                    loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_2);

                                                    function loadckEditor_2() {
                                                        loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
                                                            $("textarea#ckEditor_2").ckeditor({
                                                                extraPlugins: "image2",
                                                                filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
                                                                filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
                                                            });
                                                        });
                                                    }
                                                </script>
                                            @endpush@php
                                                if(isset($data)){
                                                    $value = is_a($data->percent,'Illuminate\Database\Eloquent\Collection') ? $data->percent->pluck('id')->toArray() : $data->percent;
                                                    $value = key_exists('percent',old()) ? old('percent') : $value;
                                                } else{
                                                    $value = old('percent');
                                                }
                                            @endphp


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                       for="selectField_3">Percent</label>
                                                <div class="col-md-9">
                                                    <select class="dd col-lg-12 form-control" id="selectField_3"
                                                            name="percent">
                                                        <option value="" class="default" selected>Select Option</option>
                                                        <option {{ $value == "0-40" ? 'selected' : '' }} value="0-40">0% -
                                                            40%
                                                        </option>
                                                        <option {{ $value == "41-80" ? 'selected' : '' }} value="41-80">
                                                            41% - 80%
                                                        </option>
                                                        <option {{ $value == "81-100" ? 'selected' : '' }} value="81-100">
                                                            81% - 100%
                                                        </option>
                                                    </select>
                                                    @if($errors->get('percent'))
                                                        <br>
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('percent') }}</div>
                                                    @endif
                                                </div>
                                            </div>


                                            @push('scripts')

                                                <script>

                                                    loadCss(['../vendor/betterfly/plugins/selectPlugin/select2.min.css']);
                                                    loadScript(['../vendor/betterfly/plugins/selectPlugin/select2.min.js'], loadselectField_3);

                                                    function loadselectField_3() {
                                                        $("select#selectField_3").select2();
                                                    }

                                                </script>

                                            @endpush
                                            <input type="hidden" value="\App\Modules\Results\ResultsRequest"
                                                   name="request_name_space">
                                            <div class=" text-right">
                                                <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                    <i class="fa fa-dot-circle-o"></i> Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection