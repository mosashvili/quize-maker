@extends('betterfly::admin.common.layout')

@section('content')

    <main class="main">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('quizzes.index') }}">Quizzes</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('quizzes.questions.index',[$quiz->id,$question->id]) }}">Questions</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('quizzes.questions.answers.index',[$quiz->id,$question->id]) }}">Answers</a>
            </li>
            <li class="breadcrumb-item active">Answer</li>
        </ol>
        @if(\Session::get('status'))
            <div class="container-fluid">
                <div id="ui-view">
                    <div class="alert alert-success" role="alert"> {{ \Session::get('status') }}</div>
                </div>
            </div>
        @endif
        <div class="container-fluid">
            <div id="ui-view">
                <div>
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Answers</strong>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal"
                                              action="{{ route("quizzes.questions.answers.store",[$quiz->id,$question->id]) }}"
                                              method="post"
                                              enctype="multipart/form-data">
                                            @csrf
                                            @php
                                                if(isset($data)){
                                                $value = $data->answer;
                                                $value = key_exists('answer',old()) ? old('answer') : $value;
                                                } else{
                                                $value = old('answer');
                                                }
                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="text-input">Answer</label>
                                                <div class="col-md-9">
        <textarea id="ckEditor_0" name="answer">
            {{ $value }}
        </textarea>
                                                    @if($errors->get('answer'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('answer') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            @push('scripts')


                                                <script>
                                                    loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

                                                    loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_0);

                                                    function loadckEditor_0() {
                                                        loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
                                                            $("textarea#ckEditor_0").ckeditor({
                                                                extraPlugins: "image2",
                                                                filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
                                                                filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
                                                            });
                                                        });
                                                    }
                                                </script>
                                            @endpush@php
                                                if(isset($data)){
                                                $value = $data->tip;
                                                $value = key_exists('tip',old()) ? old('tip') : $value;
                                                } else{
                                                $value = old('tip');
                                                }
                                            @endphp

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="text-input">Tip</label>
                                                <div class="col-md-9">
        <textarea id="ckEditor_1" name="tip">
            {{ $value }}
        </textarea>
                                                    @if($errors->get('tip'))
                                                        <br>
                                                        <div class="alert alert-danger"
                                                             role="alert">{{ $errors->first('tip') }}</div>
                                                    @endif
                                                </div>
                                            </div>

                                            @push('scripts')


                                                <script>
                                                    loadCss('../vendor/betterfly/plugins/ckeditor/ckEditorSamples.css');

                                                    loadScript(['../vendor/betterfly/plugins/ckeditor/ckeditor.js'], loadckEditor_1);

                                                    function loadckEditor_1() {
                                                        loadScript(['../vendor/betterfly/plugins/ckeditor/adapters/jquery.js'], function () {
                                                            $("textarea#ckEditor_1").ckeditor({
                                                                extraPlugins: "image2",
                                                                filebrowserBrowseUrl: "{{ route('ckfinder_browser') }}",
                                                                filebrowserUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Files"
                                                            });
                                                        });
                                                    }
                                                </script>
                                            @endpush@php
                                                if(isset($data)){
                                                    $value = $data->correct;
                                                    $value = key_exists('correct',old()) ? old('correct') : $value;
                                                } else{
                                                    $value = old('correct');
                                                }
                                            @endphp


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                       for="checkbox-input">Correct</label>
                                                <div class="col-md-9">
                                                    <label class="switch switch-label switch-outline-success-alt">
                                                        <input class="switch-input checkbox-plugin" for="correct"
                                                               type="checkbox" {{ $value ? 'checked' : '' }}>
                                                        <span class="switch-slider" data-checked="On"
                                                              data-unchecked="Off"></span>
                                                    </label>
                                                    <input type="hidden" name="correct" value="{{ $value }}">
                                                </div>
                                                @if($errors->get('correct'))
                                                    <br>
                                                    <div class="alert alert-danger"
                                                         role="alert">{{ $errors->first('correct') }}</div>
                                                @endif
                                            </div>
                                            <input type="hidden" value="true"
                                                   name="multilang">
                                            <input type="hidden" value="\App\Modules\Answers\AnswersRequest"
                                                   name="request_name_space">
                                            <div class=" text-right">
                                                <button class="btn btn-sm btn-success btn-primary" type="submit">
                                                    <i class="fa fa-dot-circle-o"></i> Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection