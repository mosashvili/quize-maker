<?php
Route::resource('gamers.history', 'App\Modules\History\HistoryController', [
    'names' => [
        'index' => 'gamers.history.index',
        'store' => 'gamers.history.store',
        'update' => 'gamers.history.update',
        'destroy' => 'gamers.history.delete'
    ]
]);
Route::get('history/quiz/{id}','App\Modules\History\HistoryController@byQuiz')->name('gamers.history.quiz');
