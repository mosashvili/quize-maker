<?php
namespace App\Modules\History;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class HistoryTransformer extends BaseTransformerAbstract
{
    public function transform($history)
    {
        $data = [
            'id' => $history->id,
            'quiz_id' => $history->quiz_id,
            'gamer_id' => $history->gamer_id,
            'result_percentage' => $history->result_percentage,
            'completion_time' => $history->completion_time,
            'correct_answer' => $history->correct_answer,
        ];

        return $data;
    }
}