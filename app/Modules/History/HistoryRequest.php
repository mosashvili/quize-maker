<?php
namespace App\Modules\History;

use BetterFly\Skeleton\App\Http\Requests\BaseFormRequest;

class HistoryRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return $this->getRule();
    }

    /**
     * Custom message for validation
     *
     * @return  array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return  array
     */
    public function filters()
    {
        return [

        ];
    }

    private function getRule()
    {
        $type = $this->getMethod();

        switch($type)
        {
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                            'quiz_id' => 'required|integer',
                            'gamer_id' => 'required|integer',
                            'result_percentage' => 'required|integer',
                            'completion_time' => 'required|max:255',
                            'correct_answer' => 'required|integer',
                        ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                            'quiz_id' => 'required|integer',
                            'gamer_id' => 'required|integer',
                            'result_percentage' => 'required|integer',
                            'completion_time' => 'required|max:255',
                            'correct_answer' => 'required|integer',
                        ];
                }
            default:break;
        }
    }
}

