<?php
namespace App\Modules\History;

use BetterFly\Skeleton\Repositories\BaseRepository;

class HistoryRepository extends BaseRepository {
    public function __construct(History $model){
        parent::__construct($model);
    }
}