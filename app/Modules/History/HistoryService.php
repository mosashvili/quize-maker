<?php
namespace App\Modules\History;


use BetterFly\Skeleton\Services\BaseService;

class HistoryService extends BaseService{

  public function __construct(HistoryRepository $repository){
    parent::__construct($repository);
  }
}