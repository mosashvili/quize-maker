<?php

namespace App\Modules\History;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Gamers\Gamers;

class HistoryController extends Controller
{
    protected $historyService;

    public function __construct(HistoryService $historyService)
    {
        $this->historyService = $historyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request, Gamers $Gamer)
    {

        $data = getGamersHistory($Gamer->id);
        if ($request->ajax())
            return $data;
        else
            return view('admin.history.index')->with([
                'data' => $data,
                'Gamer' => $Gamer
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create(Gamers $Gamer)
    {
        return view('admin.history.create')->with([
            'Gamer' => $Gamer
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(HistoryRequest $request, Gamers $Gamer)
    {

        $request->merge(['Gamer' => $Gamer->id]);
        $item = $this->historyService->create($request->input());
        if ($request->ajax()) {
            return $item;
        } else {
            \Session::flash('status', 'Successfully created');
            return redirect()->route('Gamers.history.index', $Gamer->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->historyService->getById($id) ?:
            response(['message' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, Gamers $Gamer, $id)
    {
        $data = $this->historyService->getById($id);
        if ($request->ajax())
            return $data;
        else
            return view('admin.history.edit')->with([
                'data' => $data,

                'Gamer' => $Gamer
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update(HistoryRequest $request, Gamers $Gamer, $id)
    {
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->historyService->update($data);

        if ($request->ajax()) {
            return $data;
        } else {
            \Session::flash('status', 'Successfully updated');
            return redirect()->route('Gamers.history.edit', [$Gamer->id, $data->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Gamers $Gamer, $id)
    {
        return $this->historyService->delete($id) ?
            response(['message' => 'Successfully deleted']) :
            response(['message' => 'Something went wrong']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function byQuiz(Request $request, $quizId)
    {

        $data = getQuizHistory($quizId);
        if ($request->ajax())
            return $data;
        else
            return view('admin.history.index')->with([
                'data' => $data
            ]);
    }
}