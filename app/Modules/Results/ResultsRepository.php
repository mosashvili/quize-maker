<?php
namespace App\Modules\Results;

use BetterFly\Skeleton\Repositories\BaseRepository;

class ResultsRepository extends BaseRepository {
    public function __construct(Results $model){
        parent::__construct($model);
    }
}