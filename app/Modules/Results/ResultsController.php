<?php

namespace App\Modules\Results;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Quizzes\Quizzes;

class ResultsController extends Controller
{
    protected $resultsService;

    public function __construct(ResultsService $resultsService)
    {
        $this->resultsService = $resultsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request, Quizzes $quiz)
    {

        $data = $quiz->results;

        if ($request->ajax())
            return $data;
        else
            return view('admin.results.index')->with([
                'data' => $data,
                'quiz' => $quiz
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create(Quizzes $quiz)
    {
        return view('admin.results.create')->with([
            'quiz' => $quiz
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(ResultsRequest $request, Quizzes $quiz)
    {

        $request->merge(['quiz' => $quiz->id]);
        $item = $this->resultsService->create($request->input());
        if ($request->ajax()) {
            return $item;
        } else {
            \Session::flash('status', 'Successfully created');
            return redirect()->route('quizzes.results.index', $quiz->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->resultsService->getById($id) ?:
            response(['message' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, Quizzes $quiz, $id)
    {
        $data = $this->resultsService->getById($id);
        if ($request->ajax())
            return $data;
        else
            return view('admin.results.edit')->with([
                'data' => $data,
                'quiz' => $quiz
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update(ResultsRequest $request, Quizzes $quiz, $id)
    {
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->resultsService->update($data);

        if ($request->ajax()) {
            return $data;
        } else {
            \Session::flash('status', 'Successfully updated');
            return redirect()->route('quizzes.results.edit', [$quiz->id, $data->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Quizzes $quiz, $id)
    {
        return $this->resultsService->delete($id) ?
            response(['message' => 'Successfully deleted']) :
            response(['message' => 'Something went wrong']);
    }
}