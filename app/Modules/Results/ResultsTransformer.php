<?php
namespace App\Modules\Results;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class ResultsTransformer extends BaseTransformerAbstract
{
    public function transform($results)
    {
        $data = [
            'id' => $results->id,
            'title' => $results->title,
            'text' => $results->text,
            'frame' => $results->frame,
            'percent' => $results->percent,
        ];

        return $data;
    }
}