<?php
namespace App\Modules\Results;


use BetterFly\Skeleton\Services\BaseService;

class ResultsService extends BaseService{

  public function __construct(ResultsRepository $repository){
    parent::__construct($repository);
  }
}