<?php
namespace App\Modules\Results;

    
use Illuminate\Database\Eloquent\Model;

class ResultsTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["title","text","frame",];


}