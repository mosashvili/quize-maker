<?php
Route::resource('quizzes.results', 'App\Modules\Results\ResultsController', [
    'names' => [
        'index' => 'quizzes.results.index',
        'store' => 'quizzes.results.store',
        'update' => 'quizzes.results.update',
        'destroy' => 'quizzes.results.delete'
    ]
]);
