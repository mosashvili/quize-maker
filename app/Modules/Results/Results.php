<?php

namespace App\Modules\Results;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class Results extends Model
{
    protected $table = 'results';
    protected $fillable = ["percent"];

    use Translatable;

    public $translationModel = 'App\Modules\Results\ResultsTranslation';
    public $translatedAttributes = ["title", "text", "frame",];

    public function quiz()
    {
        return $this->belongsTo('App\Modules\Quizzes\Quizzes','quizze_id');
    }
}