<?php
namespace App\Modules\Answers;


use BetterFly\Skeleton\Services\BaseService;

class AnswersService extends BaseService{

  public function __construct(AnswersRepository $repository){
    parent::__construct($repository);
  }
}