<?php

namespace App\Modules\Answers;

use Astrotomic\Translatable\Translatable;
use Kalnoy\Nestedset\NodeTrait;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    protected $table = 'answers';
    use NodeTrait;
    protected $fillable = ["correct", "id"];

    use Translatable;

    public $translationModel = 'App\Modules\Answers\AnswersTranslation';
    public $translatedAttributes = ["answer", "tip",];

//    public function quiz()
//    {
//        return $this->belongsTo('App\Modules\Quizzes\Quizzes','quizze_id');
//    }

    public function question()
    {
        return $this->belongsTo('App\Modules\Questions\Questions','question_id');
    }
}