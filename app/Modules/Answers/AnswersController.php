<?php

namespace App\Modules\Answers;

use App\Http\Controllers\Controller;
use App\Modules\Questions\Questions;
use App\Modules\Quizzes\Quizzes;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    protected $answersService;

    public function __construct(AnswersService $answersService)
    {
        $this->answersService = $answersService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request,Quizzes $quiz,Questions $question)
    {

        $data = Answers::where('question_id','=',$question->id)->defaultOrder()->get();

        if ($request->ajax())
            return $data;
        else
            return view('admin.answers.index')->with([
                'quiz' => $quiz,
                'data' => $data,
                'question' => $question
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create(Quizzes $quiz,Questions $question)
    {
        return view('admin.answers.create')->with([
            'question' => $question,
            'quiz' => $quiz
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(AnswersRequest $request, Quizzes $quiz,Questions $question)
    {

        $request->merge(['question' => $question->id]);
        $item = $this->answersService->create($request->input());
        if ($request->ajax()) {
            return $item;
        } else {
            \Session::flash('status', 'Successfully created');
            return redirect()->route('quizzes.questions.answers.index', [$quiz->id, $question->id ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->answersService->getById($id) ?:
            response(['message' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, Quizzes $quiz,Questions $question, $id)
    {
        $data = $this->answersService->getById($id);
        if ($request->ajax())
            return $data;
        else
            return view('admin.answers.edit')->with([
                'data' => $data,
                'quiz'=> $quiz,
                'question' => $question
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update(AnswersRequest $request, Quizzes $quiz, Questions $question, $id)
    {
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->answersService->update($data);

        if ($request->ajax()) {
            return $data;
        } else {
            \Session::flash('status', 'Successfully updated');
            return redirect()->route('quizzes.questions.answers.edit', [$quiz->id,$question->id, $data->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Quizzes $quiz,Questions $question, $id)
    {
        return $this->answersService->delete($id) ?
            response(['message' => 'Successfully deleted']) :
            response(['message' => 'Something went wrong']);
    }
}