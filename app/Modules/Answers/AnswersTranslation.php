<?php
namespace App\Modules\Answers;

    
use Illuminate\Database\Eloquent\Model;

class AnswersTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["answer","tip",];


}