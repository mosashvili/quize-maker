<?php
namespace App\Modules\Answers;

use BetterFly\Skeleton\Repositories\BaseRepository;

class AnswersRepository extends BaseRepository {
    public function __construct(Answers $model){
        parent::__construct($model);
    }
}