<?php
namespace App\Modules\Answers;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class AnswersTransformer extends BaseTransformerAbstract
{
    public function transform($answers)
    {
        $data = [
            'id' => $answers->id,
            'answer' => $answers->answer,
            'tip' => $answers->tip,
            'correct' => $answers->correct,
        ];

        return $data;
    }
}