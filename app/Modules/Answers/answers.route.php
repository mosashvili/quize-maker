<?php
Route::resource('quizzes.questions.answers', 'App\Modules\Answers\AnswersController', [
    'names' => [
        'index' => 'quizzes.questions.answers.index',
        'store' => 'quizzes.questions.answers.store',
        'update' => 'quizzes.questions.answers.update',
        'destroy' => 'quizzes.questions.answers.delete'
    ]
]);
