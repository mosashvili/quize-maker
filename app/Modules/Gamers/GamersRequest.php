<?php
namespace App\Modules\Gamers;

use BetterFly\Skeleton\App\Http\Requests\BaseFormRequest;

class GamersRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return $this->getRule();
    }

    /**
     * Custom message for validation
     *
     * @return  array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return  array
     */
    public function filters()
    {
        return [

        ];
    }

    private function getRule()
    {
        $type = $this->getMethod();

        switch($type)
        {
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                            'name_lastname' => 'required|max:255',
                            'email' => 'required|max:255',
                            'phone' => 'required|max:255',
                            'id_number' => 'required|max:255',
                            'fb_profile' => 'nullable|max:255',
                            'age' => 'nullable|integer',
                        ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                            'name_lastname' => 'required|max:255',
                            'email' => 'required|max:255',
                            'phone' => 'required|max:255',
                            'id_number' => 'required|max:255',
                            'fb_profile' => 'nullable|max:255',
                            'age' => 'nullable|integer',
                        ];
                }
            default:break;
        }
    }
}

