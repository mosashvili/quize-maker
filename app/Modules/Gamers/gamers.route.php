<?php
Route::resource('gamers', 'App\Modules\Gamers\GamersController', [
    'names' => [
        'index' => 'gamers.index',
        'store' => 'gamers.store',
        'update' => 'gamers.update',
        'destroy' => 'gamers.delete'
    ]
]);
