<?php
namespace App\Modules\Gamers;


use BetterFly\Skeleton\Services\BaseService;

class GamersService extends BaseService{

  public function __construct(GamersRepository $repository){
    parent::__construct($repository);
  }
}