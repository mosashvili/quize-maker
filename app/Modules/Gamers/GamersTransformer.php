<?php
namespace App\Modules\Gamers;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class GamersTransformer extends BaseTransformerAbstract
{
    public function transform($gamers)
    {
        $data = [
            'id' => $gamers->id,
            'name_lastname' => $gamers->name_lastname,
            'email' => $gamers->email,
            'phone' => $gamers->phone,
            'id_number' => $gamers->id_number,
            'fb_profile' => $gamers->fb_profile,
            'age' => $gamers->age,
        ];

        return $data;
    }
}