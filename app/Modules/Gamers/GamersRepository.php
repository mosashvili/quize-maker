<?php
namespace App\Modules\Gamers;

use BetterFly\Skeleton\Repositories\BaseRepository;

class GamersRepository extends BaseRepository {
    public function __construct(Gamers $model){
        parent::__construct($model);
    }
}