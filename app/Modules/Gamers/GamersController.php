<?php

namespace App\Modules\Gamers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GamersController extends Controller
{
    protected $gamersService;

    public function __construct(GamersService $gamersService)
    {
        $this->gamersService = $gamersService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = $this->gamersService->getList(['paginate' => 10]);
        if ($request->ajax())
            return $data;
        else
            return view('admin.gamers.index')->with([
                'data' => $data,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gamers.create')->with([

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(GamersRequest $request)
    {

        $item = $this->gamersService->create($request->input());
        if ($request->ajax()) {
            return $item;
        } else {
            \Session::flash('status', 'Successfully created');
            return redirect()->route('gamers.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->gamersService->getById($id) ?:
            response(['message' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = $this->gamersService->getById($id);
        if ($request->ajax())
            return $data;
        else
            return view('admin.gamers.edit')->with([
                'data' => $data,


            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update(GamersRequest $request, $id)
    {
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->gamersService->update($data);

        if ($request->ajax()) {
            return $data;
        } else {
            \Session::flash('status', 'Successfully updated');
            return redirect()->route('gamers.edit', [$data->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->gamersService->delete($id) ?
            response(['message' => 'Successfully deleted']) :
            response(['message' => 'Something went wrong']);
    }
}