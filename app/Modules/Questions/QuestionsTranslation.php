<?php
namespace App\Modules\Questions;

    
use Illuminate\Database\Eloquent\Model;

class QuestionsTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["question",];


}