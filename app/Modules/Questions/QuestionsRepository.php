<?php
namespace App\Modules\Questions;

use BetterFly\Skeleton\Repositories\BaseRepository;

class QuestionsRepository extends BaseRepository {
    public function __construct(Questions $model){
        parent::__construct($model);
    }
}