<?php
Route::resource('quizzes.questions', 'App\Modules\Questions\QuestionsController', [
    'names' => [
        'index' => 'quizzes.questions.index',
        'store' => 'quizzes.questions.store',
        'update' => 'quizzes.questions.update',
        'destroy' => 'quizzes.questions.delete'
    ]
]);
