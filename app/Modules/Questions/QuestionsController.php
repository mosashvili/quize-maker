<?php

namespace App\Modules\Questions;

use App\Http\Controllers\Controller;
use App\Modules\Quizzes\Quizzes;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    protected $questionsService;

    public function __construct(QuestionsService $questionsService)
    {
        $this->questionsService = $questionsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request, Quizzes $quiz)
    {

        $data = Questions::where('quizze_id','=',$quiz->id)->defaultOrder()->get();

        if ($request->ajax())
            return $data;
        else
            return view('admin.questions.index')->with([
                'data' => $data,
                'quiz' => $quiz
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create(Quizzes $quiz)
    {
        return view('admin.questions.create')->with([
            'quiz' => $quiz
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(QuestionsRequest $request, Quizzes $quiz)
    {
        $request->merge(['quiz' => $quiz->id]);
        $item = $this->questionsService->create($request->input());
        if ($request->ajax()) {
            return $item;
        } else {
            \Session::flash('status', 'Successfully created');
            return redirect()->route('quizzes.questions.index', $quiz->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->questionsService->getById($id) ?:
            response(['message' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, Quizzes $quiz, $id)
    {
        $data = $this->questionsService->getById($id);
        if ($request->ajax())
            return $data;
        else
            return view('admin.questions.edit')->with([
                'data' => $data,

                'quiz' => $quiz
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update(QuestionsRequest $request, Quizzes $quiz, $id)
    {
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->questionsService->update($data);

        if ($request->ajax()) {
            return $data;
        } else {
            \Session::flash('status', 'Successfully updated');
            return redirect()->route('quizzes.questions.edit', [$quiz->id, $data->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Quizzes $quiz, $id)
    {
        return $this->questionsService->delete($id) ?
            response(['message' => 'Successfully deleted']) :
            response(['message' => 'Something went wrong']);
    }
}