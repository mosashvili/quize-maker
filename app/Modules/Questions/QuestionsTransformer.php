<?php
namespace App\Modules\Questions;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class QuestionsTransformer extends BaseTransformerAbstract
{
    public function transform($questions)
    {
        $data = [
            'id' => $questions->id,
            'name' => $questions->name,
            'question' => $questions->question,
        ];

        return $data;
    }
}