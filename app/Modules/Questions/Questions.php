<?php

namespace App\Modules\Questions;

use Astrotomic\Translatable\Translatable;
use Kalnoy\Nestedset\NodeTrait;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = 'questions';
    use NodeTrait;
    protected $fillable = ["id","layout"];

    use Translatable;

    public $translationModel = 'App\Modules\Questions\QuestionsTranslation';
    public $translatedAttributes = ["name", "question"];


    public function quiz()
    {
        return $this->belongsTo('App\Modules\Quizzes\Quizzes','quizze_id');
    }

    public function answers()
    {
        return $this->hasMany('App\Modules\Answers\Answers','question_id');
    }
}