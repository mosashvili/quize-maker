<?php
namespace App\Modules\Questions;


use BetterFly\Skeleton\Services\BaseService;

class QuestionsService extends BaseService{

  public function __construct(QuestionsRepository $repository){
    parent::__construct($repository);
  }
}