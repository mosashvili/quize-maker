<?php
namespace App\Modules\Quizzes;


use BetterFly\Skeleton\Services\BaseService;

class QuizzesService extends BaseService{

  public function __construct(QuizzesRepository $repository){
    parent::__construct($repository);
  }
}