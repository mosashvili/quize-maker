<?php
Route::resource('quizzes', 'App\Modules\Quizzes\QuizzesController', [
    'names' => [
        'index' => 'quizzes.index',
        'store' => 'quizzes.store',
        'update' => 'quizzes.update',
        'destroy' => 'quizzes.delete'
    ]
]);
