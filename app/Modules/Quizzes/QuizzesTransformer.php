<?php
namespace App\Modules\Quizzes;

use BetterFly\Skeleton\App\Http\Transformers\BaseTransformerAbstract;
use Illuminate\Support\Facades\Auth;

class QuizzesTransformer extends BaseTransformerAbstract
{
    public function transform($quizzes)
    {
        $data = [
            'id' => $quizzes->id,
            'name' => $quizzes->name,
            'description' => $quizzes->description,
            'image' => $quizzes->image,
            'register' => $quizzes->register,
            'start_time' => $quizzes->start_time,
        ];

        return $data;
    }
}