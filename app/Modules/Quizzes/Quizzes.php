<?php

namespace App\Modules\Quizzes;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class Quizzes extends Model
{
    protected $table = 'quizzes';
    protected $fillable = ["register", "start_time","visibility"];

    use Translatable;

    public $translationModel = 'App\Modules\Quizzes\QuizzesTranslation';
    public $translatedAttributes = ["name", "description", "image","frame"];

    public function questions()
    {
        return $this->hasMany('App\Modules\Questions\Questions','quizze_id');
    }

    public function results()
    {
        return $this->hasMany('App\Modules\Results\Results','quizze_id');
    }

}