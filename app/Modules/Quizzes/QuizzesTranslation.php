<?php
namespace App\Modules\Quizzes;

    
use Illuminate\Database\Eloquent\Model;

class QuizzesTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ["name","description","image","visibility","frame"];


}