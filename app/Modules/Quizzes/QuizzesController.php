<?php

namespace App\Modules\Quizzes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuizzesController extends Controller
{
    protected $quizzesService;

    public function __construct(QuizzesService $quizzesService)
    {
        $this->quizzesService = $quizzesService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = $this->quizzesService->getList(['paginate' => 10]);
        if ($request->ajax())
            return $data;
        else
            return view('admin.quizzes.index')->with([
                'data' => $data,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.quizzes.create')->with([

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @return  \Illuminate\Http\Response
     */
    public function store(QuizzesRequest $request)
    {

        $item = $this->quizzesService->create($request->input());
        if ($request->ajax()) {
            return $item;
        } else {
            \Session::flash('status', 'Successfully created');
            return redirect()->route('quizzes.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->quizzesService->getById($id) ?:
            response(['message' => 'Record not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = $this->quizzesService->getById($id);
        if ($request->ajax())
            return $data;
        else
            return view('admin.quizzes.edit')->with([
                'data' => $data,


            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request $request
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function update(QuizzesRequest $request, $id)
    {
        $data = $request->input();
        $data['id'] = $id;

        $data = $this->quizzesService->update($data);

        if ($request->ajax()) {
            return $data;
        } else {
            \Session::flash('status', 'Successfully updated');
            return redirect()->route('quizzes.edit', [$data->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->quizzesService->delete($id) ?
            response(['message' => 'Successfully deleted']) :
            response(['message' => 'Something went wrong']);
    }
}