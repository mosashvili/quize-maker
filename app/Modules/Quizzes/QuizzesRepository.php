<?php
namespace App\Modules\Quizzes;

use BetterFly\Skeleton\Repositories\BaseRepository;

class QuizzesRepository extends BaseRepository {
    public function __construct(Quizzes $model){
        parent::__construct($model);
    }
}