<?php
namespace App\Modules\Quizzes;

use BetterFly\Skeleton\App\Http\Requests\BaseFormRequest;

class QuizzesRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return $this->getRule();
    }

    /**
     * Custom message for validation
     *
     * @return  array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return  array
     */
    public function filters()
    {
        return [

        ];
    }

    private function getRule()
    {
        $type = $this->getMethod();

        switch($type)
        {
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                            'name' => 'required|max:255',
                            'description' => 'nullable|string',
                            'frame' => 'nullable|string',
                            'register' => 'nullable|max:255',
                            'start_time' => 'nullable|max:255',
                            'visibility' => 'nullable|string'
                        ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                            'name' => 'required|max:255',
                            'description' => 'nullable|string',
                            'frame' => 'nullable|string',
                            'register' => 'nullable|max:255',
                            'start_time' => 'nullable|max:255',
                            'visibility' => 'nullable|string'
                        ];
                }
            default:break;
        }
    }
}

