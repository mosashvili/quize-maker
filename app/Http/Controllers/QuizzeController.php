<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Modules\Quizzes\Quizzes;
use Illuminate\Support\Facades\Mail;
use App\Modules\Gamers\Gamers;
use Illuminate\Support\Facades\DB;
use App\Modules\History\History;

class QuizzeController extends Controller
{
    public function index($quizzeId){
        $quizze = Quizzes::find($quizzeId);

        if(!$quizze)
            return response(['success' => false,'message' => 'Quizze Not Found']);

        if($quizze->register){
            return view('register')->with(['quizze' => $quizze]);
        }

        return redirect(route('game', [$quizzeId,0]));
    }

    public function register(Request $request){
        $quizze = Quizzes::find($request->input('quizz_id'));

        if(!$quizze)
            return response(['success' => false,'message' => 'Quizze Not Found']);

        $gamer = [
            'name_lastname' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'id_number' => $request->input('id'),
            'fb_profile' => $request->input('profile'),
            'age' => $request->input('age'),
        ];

        $gamerInBase = Gamers::where('email','=',$gamer['email'])->orWhere('id_number','=',$gamer['id_number'])->first();
        
        if($gamerInBase !== null)
            return $this->sendCode($gamerInBase,$request->input('quizz_id'),$quizze);

        $gamer = new Gamers($gamer);
        $gamer->save();

        return $this->sendCode($gamer,$request->input('quizz_id'),$quizze);
    }

    public function sendCode($gamer,$quizzeId,$quizze){
        $startTime = new DateTime($quizze->start_time);
        $now = new DateTime();

        $code = $this->generateCode($gamer,$quizzeId);

        if(!$code)
            return response(['message' => 'You already played this quiz','showLogin' => false, 'success' => false]);

        $data = ['name' => 'Millab '.$quizze->name, "body" => '<img src="'.url('/img/mail.jpg').'"><br><br>რეგისტრაციის კოდი: ' . $code.'<br><br> შეინახე რეგისტრაციის კოდი '.$quizze->start_time.' აღნიშნული კოდით გაიარე ავტორიზაცია,<br> რაც შესაძლებლობას მოგცემს, მონაწილეობა მიიღო ქვიზების კონკურსში და მოიგო სპეციალური პრიზები.<br><br> გთხოვთ არ უპასუხოთ ამ მეილს.','gamer' => $gamer,'quizze' => $quizze];

        try {
            Mail::send([], [], function ($message) use ($data) {
                $message->to($data['gamer']->email)
                    ->from('quizzes@millab.ge')
                    ->subject($data['quizze']->name)
                    ->setContentType('text/html')
                    ->setBody($data['body']);
            });

            if($startTime < $now) {
                return response(['message' => 'კოდი წარმატებით გაიგზავნა მეილზე','showLogin' => true, 'success' => true]);
            }else{
                return response(['message' => 'კოდი წარმატებით გაიგზავნა მეილზე','showLogin' => false, 'success' => true]);
            }

        } catch (\Exception $e) {
            return response(['message' => 'Unable to Send Email,try again later','showLogin' => false, 'success' => false]);
        }


    }

    public function login(Request $request)
    {

        $existCodeQuery = DB::table('quizz_codes')->where([
            ['code', '=', $request->input('code')],
            ['quizze_id', '=', $request->input('quizz_id')]
        ]);
        $existCode = $existCodeQuery->first();

        if($existCode === null)
            return response(['message' => 'Invalid Code', 'success' => false]);

        if($existCode->used)
            return response(['message' => 'Code Already Used', 'success' => false]);

        if(!$existCodeQuery->update(['used' => 1]))
            return response([ 'message' => 'System Error. Try Again Later','success' => false]);

        return response(['link' => route('game', [$existCode->quizze_id,$existCode->gamer_id]), 'message' => 'Invalid Code', 'success' => true]);
    }

    public function generateCode($gamer, $quizzeId)
    {
        $existGamerCode = DB::table('quizz_codes')->where([
            ['quizze_id', '=', $quizzeId],['gamer_id','=', $gamer->id]
        ])->first();

        if($existGamerCode){
            if($existGamerCode->used)
                return false;

            DB::table('quizz_codes')->where([
                ['quizze_id', '=', $quizzeId],['gamer_id','=', $gamer->id]
            ])->delete();
        }

        do{
            $code = rand(1000,9999);
            $existCode = DB::table('quizz_codes')->where([
                ['code','=', $code],['quizze_id','=', $quizzeId]
            ])->get();
        }while(!$existCode->isEmpty());

        DB::table('quizz_codes')->insert(
            ['gamer_id' => $gamer->id, 'quizze_id' => $quizzeId,'code' => $code]
        );

        return $code;
    }

    public function game(Quizzes $quizze,$gamer = false){

        if($gamer){
            $gamer = Gamers::find($gamer);
            $alreadyPlayed = DB::table('history')->where([
                ['gamer_id','=', $gamer->id],
                ['quiz_id','=', $quizze->id]
            ])->get();
            if(!$alreadyPlayed->isEmpty()){
                return redirect(route('quiz',$quizze->id));
            }
        }

        return view('game')->with(['gamer' => $gamer,'quiz' => $quizze]);
    }

    public function addHistory(Request $request){
        DB::table('history')->insert(
            [
                'result_percentage' => $request->input("percent"),
                'correct_answer' => $request->input("point"),
                'completion_time' => $request->input("time"),
                'gamer_id' => $request->input("gamer"),
                'quiz_id' => $request->input("quiz_id"),
            ]
        );
    }

    // public function resendCodes(){
    //     $gamers = Gamers::where('sended' ,'!=',1)->get();
    //     $quizze = Quizzes::find(19);

    //     foreach ($gamers as $gamer) {
    //         $res = $this->sendCode($gamer,$quizze->id,$quizze);
    //         if($res->original['success']){
    //             $gamer->sended = 1;
    //             $gamer->save();
    //         }
    //     }
    // }
}
