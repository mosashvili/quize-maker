<?php
use Illuminate\Support\Facades\DB;

if (! function_exists('getGamersHistory')) {
    function getGamersHistory($gamerId) {
       $gamerHistory = DB::table('history as h')
           ->where('gamer_id',$gamerId)
           ->join('gamers as g','h.gamer_id','=','g.id')
           ->join('quizzes_translations as qt','h.quiz_id','=','qt.quizzes_id')
           ->join('questions as q','q.quizze_id','=','h.quiz_id')
           ->where('qt.locale','=', App()->getLocale())
           ->select(['g.name_lastname','g.phone','qt.name as quiz_name','h.*',DB::raw('COUNT(q.id) as questions_sum')])
           ->groupBy('h.id')
           ->get();

       return $gamerHistory;
    }
}

if (! function_exists('getQuizHistory')) {
    function getQuizHistory($quizId) {
        $quizHistory = DB::table('history as h')
            ->where('quiz_id',$quizId)
            ->join('gamers as g','g.id','=','h.gamer_id')
            ->join('quizzes_translations as qt','h.quiz_id','=','qt.quizzes_id')
            ->join('questions as q','h.quiz_id','=','q.quizze_id')
            ->where('qt.locale','=', App()->getLocale())
            ->select(['g.name_lastname','g.phone','qt.name as quiz_name','h.*',DB::raw('COUNT(q.id) as questions_sum')])
            ->groupBy('h.id')
            ->get();

        return $quizHistory;
    }
}
//DB::raw('ROUND((correct_answer * 100) / COUNT(q.id),1)  as result_percentage')