window.App = {

    Init() {
        this.sss();
    },

    sss: function () {
        // console.log('ddd');
    }

};

function countDown(date){
    var countDownDate = new Date(date).getTime();

// Update the count down every 1 second
    var x = setInterval(function() {
        var now = new Date().getTime();

        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);


        $(".countdown").html(days + ':' + hours + ":"+ minutes + ":" + seconds);

        if (distance < 0) {
            clearInterval(x);
            $(".countdown").html("00:00:00:00");
        }
    }, 1000);
}

$(document).ready(function () {
    App.Init();

    $('.authorization').click(function(){
        $('.auth-popup').addClass('visible');
    });

    $('.auth-popup .close-btn').click(function(){
        $('.auth-popup').removeClass('visible');
    });

    $('.register-now').click(function(){
        $(this).parents('.preview').fadeOut();
        $('.game-content .register-popup').addClass('visible')
    })


    if($('.countdown').length){
        var fromTime = $('.countdown').data('date').replace(/\s/, 'T');

        countDown(fromTime);
    }

    $('.register-popup .close-btn').click(function(){
        $(this).parents('.register-popup').removeClass('visible');
        $('.preview').fadeIn();
    })
});
