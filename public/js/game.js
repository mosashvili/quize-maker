var game = {
    steps: $("[data-steps]").data('steps'),
    stepsElSelector: ".game-box .step",
    currentStep: 1,
    nextBtnEl: $('.game-box .next-btn'),
    answerEl: $('.game-box .answer'),
    currentSetpEl: null,
    time: 0,
    timeInterval: null,
    callBack: null,
    point: 0,
    incorrectPoints: 0,
    answeredQuestions: 0,
    register: $("[data-register]").data('register'),

    init: function (callBack) {

        var self = this;
        this.nextBtnEl.click(function () {
            self.nextStep();
        });
        this.answerEl.click(function () {

            self.doAnswer($(this))
        });
        if (self.register)
            self.startTimer()
        else
            self.bindStatisticEvent();

        self.currentSetpEl = $('.step.step-1');

        self.callBack = callBack
    },

    nextStep: function () {
        if (!this.currentSetpEl.find('.choosed').length)
            return false;

        if (this.currentStep == this.steps)
            return this.finishGame();

        $(this.stepsElSelector + '.step-' + this.currentStep).fadeOut(0)
        $(this.stepsElSelector + '.step-' + this.currentStep).remove();

        this.currentStep += 1;

        $(this.stepsElSelector + '.step-' + this.currentStep).fadeIn(0)
        $('.game-box .game-steps-box p strong').text(this.currentStep)
        $('.game-box .game-steps-box span').removeClass('active')
        $('.game-box .game-steps-box span:nth-child(' + this.currentStep + ')').addClass('active')
        this.currentSetpEl = $('.step.step-' + this.currentStep)

    },
    doAnswer: function (el) {
        var answerKey = el.data('key');
        if (this.currentSetpEl.find('.choosed').length)
            return false;

        this.currentSetpEl.find('.answer').addClass('choosed');
        this.currentSetpEl.find('.answer[data-true=1]').addClass('true')

        this.answeredQuestions += 1;
        if (el.data("true")) {
            this.point += 1;
        } else {
            this.incorrectPoints += 1;
            el.addClass('false')
        }
        this.currentSetpEl.find('.tip-' + answerKey).removeClass('xl-none');
    },
    bindStatisticEvent: function () {
        var self = this;
        window.addEventListener('beforeunload', function (e) {

            if (self.currentStep > 1) self.sendFormData();
            e.returnValue = '';
        });
    },
    sendFormData: function () {
        var quizzeName = $('.game-title').text();
        var language = $('html').attr('lang');
        var self = this;

        $.ajax({
            url: "https://docs.google.com/forms/d/e/1FAIpQLSccFhhSKkPRFQhkrhlto-u-UxwRKt6vgi_Loih80x4IXf7kLQ/formResponse",
            data: {
                "entry.1184773079": self.answeredQuestions,
                "entry.1528965251": self.point,
                "entry.1761748254": language,
                "entry.1913201822": quizzeName,
                'entry.1018080763': self.incorrectPoints
            },
            type: "POST",
            dataType: "xml"
        });
        messageSent = true;
    },
    startTimer: function () {
        var self = this;
        self.timeInterval = setInterval(function () {
            self.time += 1;
        }, 1000)
    },

    finishGame: function () {
        var self = this;
        $(this.stepsElSelector + '.step-' + this.currentStep + ", .game-box .game-steps-box, .game-box .next-btn").fadeOut(0)
        $(this.stepsElSelector + '.step-' + this.currentStep + ", .game-box .game-steps-box, .game-box .next-btn").remove();

        var resultPercent = ((this.point * 100) / this.steps).toFixed(1);

        if (self.register) {
            clearInterval(this.timeInterval);
            var time = new Date(this.time * 1000).toISOString().substr(11, 8);
            this.callBack(this.point, time, resultPercent)
        }


        if (resultPercent >= 0 && resultPercent <= 40) {
            resultPercent = "0-40";
        } else if (resultPercent > 40 && resultPercent <= 80) {
            resultPercent = "41-80";
        } else if (resultPercent > 80 && resultPercent <= 100) {
            resultPercent = "81-100";
        }
        self.sendFormData();

        $('.result-' + resultPercent).removeClass("xl-none");

        $('.game-box .results .result .rate span').text(this.point);
    },

};
